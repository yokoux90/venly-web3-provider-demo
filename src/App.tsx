import React, { useRef } from "react";
import Web3 from "web3";
import { Venly } from "@venly/web3-provider";
import { Signer, VenlyConnect } from "@venly/connect";

function App() {
  const connection = useRef<VenlyConnect>();

  const onConnect = () => {
    console.log("Waiting...");
    // 创建Engine后Venly作为全局对象注入到浏览器
    Venly.createProviderEngine({
      clientId: "Testaccount",
      environment: "staging",
      windowMode: "POPUP",
      skipAuthentication: false,
    }).then((provider) => {
      // provider的直接用法
      const web3 = new Web3(provider);
      web3.eth.getAccounts(console.log);

      // 创建venlyConnect实例，挂载在Venly对象下
      connection.current = Venly.connect();
      console.log("Connected");
    });
  };

  const onGetProfile = () => {
    // 通过api获取信息
    connection.current &&
      connection.current.api.getProfile().then((profile) => {
        console.log(
          `Users email, ${profile.email}, has been successfully executed!`
        );
      });
  };

  const onSignMessage = () => {
    // 1. 获取wallet信息
    connection.current &&
      connection.current.api.getWallets().then((wallets) => {
        console.log("[wallets] ", wallets);
        // 2. 获取第一个wallet
        const wallet = wallets[0];
        // 3. 创建singer
        const signer: Signer | undefined = connection.current?.createSigner();
        // 4. 签名
        signer &&
          signer
            .signMessage({
              walletId: wallet.id, // 5. 必须
              secretType: wallet.secretType, // 6. 必须
              data: "I agree with terms and conditions",
            })
            .then((msg) => alert(JSON.stringify(msg)));
      });
  };

  const onDisconnect = () => {
    connection.current && connection.current.logout();
    alert("Logout");
  };

  return (
    <div className="App">
      <button onClick={onConnect}>Connect</button>
      <button onClick={onGetProfile}>GetProfile</button>
      <button onClick={onSignMessage}>SignMessage</button>
      <button onClick={onDisconnect}>Disconnect</button>
    </div>
  );
}

export default App;
